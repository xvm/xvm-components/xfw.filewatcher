// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors

//
// Imports
//

// stdlib
#include <map>
#include <string>
#include <thread>

// pybind
#include <pybind11/pybind11.h>

// xfw.filewatcher
#include "fileWatcher.h"



//
// Globals
//

std::map<std::string, FileWatcher*> watcherList;



//
// Functions
//

bool python_watcher_is_exists(const std::string& watcher_id)
{
    return watcherList.count(watcher_id) > 0U;
}


bool python_watcher_is_running(const std::string& watcher_id)
{
    if (!watcherList.count(watcher_id))
    {
        return false;
    }

    return watcherList.at(watcher_id)->IsRunning();
}


bool python_watcher_add(const std::string& watcher_id, const std::wstring& directory, const std::string& event_name, bool stop_after_event)
{
    if (watcherList.count(watcher_id)){
       return false;
    }

    watcherList.insert({watcher_id, new FileWatcher(watcher_id, directory, event_name, stop_after_event)});
    return true;
}


bool python_watcher_delete(const std::string& watcher_id)
{
    if (watcherList.count(watcher_id))
    {
        delete watcherList[watcher_id];
        watcherList.erase(watcher_id);
        return true;
    }

    return false;
}


void python_watcher_delete_all()
{
    for (auto& item : watcherList)
    {
        delete item.second;
        watcherList.erase(item.first);
    }
}


bool python_watcher_start(const std::string& watcher_id)
{
    if (watcherList.count(watcher_id))
    {
        watcherList[watcher_id]->StartWatch();
        return true;
    }

    return false;
}


void python_watcher_start_all()
{
    for (auto& item : watcherList)
    {
        item.second->StartWatch();
    }
}


bool python_watcher_stop(const std::string& watcher_id)
{
    if (watcherList.count(watcher_id))
    {
        watcherList[watcher_id]->StopWatch();
        return true;
    }

    return false;
}


void python_watcher_stop_all()
{
    for (auto& item : watcherList)
    {
        item.second->StopWatch();
    }
}



//
// Module
//

PYBIND11_MODULE(XFW_FileWatcher, m) {
    m.doc() = "XFW FileWatcher module";
    
    m.def("watcher_is_exists", &python_watcher_is_exists);
    m.def("watcher_is_running", &python_watcher_is_running);
    
    m.def("watcher_add", &python_watcher_add);
    
    m.def("watcher_delete", &python_watcher_delete);
    m.def("watcher_delete_all", &python_watcher_delete_all);

    m.def("watcher_start", &python_watcher_start);
    m.def("watcher_start_all", &python_watcher_start_all);

    m.def("watcher_stop", &python_watcher_stop);
    m.def("watcher_stopall", &python_watcher_stop_all);
}

// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors

#pragma once

#include <string>
#include <thread>

#include <Windows.h>

#define CANCELATION_CHECK_TIME 500

class FileWatcher {

public:
    FileWatcher(std::string ID, std::wstring Directory, std::string PythonCommand, bool StopWatcherAfterEvent);
    ~FileWatcher();

    void StartWatch();
    void StopWatch();

    bool IsRunning();

private:
    std::string  ID;
    std::wstring Directory;
    std::string  PythonCommand;
    bool         StopWatcherAfterEvent;

    std::thread Thread;
    HANDLE Handles[2] {0};

    void WorkingThread();

    bool PrepareFileHandle();
    bool CloseFileHandle();

    bool isRunning;
};
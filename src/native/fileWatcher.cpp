// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors


#include "fileWatcher.h"

#include <Windows.h>

#include <pybind11/pybind11.h>

FileWatcher::FileWatcher(std::string ID, std::wstring Directory, std::string PythonCommand, bool StopWatcherAfterEvent)
{
    this->ID = ID;
    this->Directory = Directory;
    this->PythonCommand = PythonCommand;
    this->StopWatcherAfterEvent = StopWatcherAfterEvent;

    this->isRunning = false;

    Handles[1] = CreateEvent(NULL, FALSE, FALSE, NULL);
}

bool FileWatcher::IsRunning()
{
    return isRunning;
}

FileWatcher::~FileWatcher()
{
    StopWatch();
    CloseHandle(Handles[1]);
}

void FileWatcher::StartWatch()
{
    if (isRunning)
    {
        return;
    }

    if (Thread.joinable())
    {
        Thread.join();
    }

    Thread = std::thread([=] {WorkingThread(); });
}

void FileWatcher::StopWatch()
{
    if (!isRunning)
    {
        return;
    }

    SetEvent(Handles[1]);
    Thread.join();
    ResetEvent(Handles[1]);
}

bool FileWatcher::PrepareFileHandle()
{
    if (Handles[0] != NULL)
    {
        return false;
    }

    Handles[0] = FindFirstChangeNotificationW(
        Directory.c_str(),
        true,
        FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_LAST_WRITE);

    if ((Handles[0] == INVALID_HANDLE_VALUE) || (Handles[0] == NULL))
    {
        return false;
    }

    return true;
}

bool FileWatcher::CloseFileHandle()
{
    if (Handles[0] == NULL)
    {
        return false;
    }

    FindCloseChangeNotification(Handles[0]);
    Handles[0] = NULL;
}

void FileWatcher::WorkingThread()
{
    isRunning = true;

    DWORD dwWaitStatus;

    if (!PrepareFileHandle())
    {
        isRunning = false;
        return;
    }

    while (true)
    {
        dwWaitStatus = WaitForMultipleObjects(2, Handles, FALSE, INFINITE);

        switch (dwWaitStatus)
        {
        case WAIT_OBJECT_0 + 0:
            CloseFileHandle();

            PyGILState_STATE gstate;
            gstate = PyGILState_Ensure();
            PyRun_SimpleStringFlags(PythonCommand.c_str(), 0);
            PyGILState_Release(gstate);

            if (StopWatcherAfterEvent)
            {
                isRunning = false;
                return;
            }

            PrepareFileHandle();
            break;

        case WAIT_OBJECT_0 + 1:
            CloseFileHandle();
            isRunning = false;
            return;
            break;

        default:
            break;
        }
    }
}

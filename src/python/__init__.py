"""
SPDX-License-Identifier: MIT
Copyright (c) 2022 XVM Contributors
"""

#
# Imports
#

# stdlib
import logging

# xfw.native
from xfw_native.python import XFWNativeModuleWrapper



#
# Globals
#

__native = None
__xfw_filewatcher_initialized = False



#
# API
#

def watcher_is_exists(watcher_id):
    """
    Checks if watcher exists

    usage: watcher_is_exists(<watcher_id>):
        watcher_id      : watcher ID

    returns: True/False
    """
    try:
        if not __native:
            return None
        return __native.watcher_is_exists(watcher_id)
    except Exception:
        logging.getLogger('XFW/FileWatcher').exception("watcher_is_exists")


def watcher_is_running(watcher_id):
    """
    Checks if watcher is running

    usage: watcher_is_running(<watcher_id>):
        watcher_id      : watcher ID

    returns: True/False
    """
    try:
        if not __native:
            return None
        return __native.watcher_is_running(watcher_id)
    except Exception:
        logging.getLogger('XFW/FileWatcher').exception("watcher_is_running")


def watcher_add(watcher_id, directory_path, event_call_text, stop_after_event=False):
    """
    Adds new directory watcher

    usage: watcher_add(<watcher_id>, <directory_path>, <event_call_text>, <stop_after_event>):
        watcher_id      : watcher ID
        directory_path  : string with directory path
        event_call_text : g_eventBus.handeEvent() function argument
        stop_after_event: stop watcher after event, False by default
    """
    try:
        if not __native:
            return None
        return __native.watcher_add(watcher_id, unicode(directory_path), event_call_text, stop_after_event)
    except Exception:
        logging.getLogger('XFW/FileWatcher').exception("watcher_add")


def watcher_delete(watcher_id):
    """
    Deletes directory watcher

    usage: watcher_delete(<watcher_id>):
        watcher_id      : watcher ID
    """
    try:
        if not __native:
            return None
        return __native.watcher_delete(watcher_id)
    except Exception:
        logging.getLogger('XFW/FileWatcher').exception("watcher_delete")


def watcher_delete_all(watcher_id):
    """
    Deletes all directory watcher

    usage: watcher_delete_all(<watcher_id>):
        watcher_id      : watcher ID
    """
    try:
        if not __native:
            return None
        return __native.watcher_delete_all(watcher_id)
    except Exception:
        logging.getLogger('XFW/FileWatcher').exception("watcher_delete_all")


def watcher_start(watcher_id):
    """
    Starts directory watcher

    usage: watcher_start(<watcher_id>):
        watcher_id      : watcher ID
    """
    try:
        if not __native:
            return None
        return __native.watcher_start(watcher_id)
    except Exception:
        logging.getLogger('XFW/FileWatcher').exception("watcher_start")


def watcher_start_all():
    """
    Starts all directory watchers

    usage: watcher_start_all():
    """
    try:
        if not __native:
            return None
        return __native.watcher_start_all()
    except Exception:
        logging.getLogger('XFW/FileWatcher').exception("watcher_start_all")


def watcher_stop(watcher_id):
    """
    Stops directory watcher

    usage: watcher_stop(<watcher_id>):
        watcher_id      : watcher ID
    """
    try:
        if not __native:
            return None
        return __native.watcher_stop(watcher_id)
    except Exception:
        logging.getLogger('XFW/FileWatcher').exception("watcher_stop")


def watcher_stop_all():
    """
    Stops all directory watchers

    usage: watcher_stop_all():
    """
    try:
        if not __native:
            return None
        return __native.watcher_stop_all()
    except Exception:
        logging.getLogger('XFW/FileWatcher').exception("watcher_stop_all")



#
# XFW Loader
#


def xfw_is_module_loaded():
    global __xfw_filewatcher_initialized
    return __xfw_filewatcher_initialized

def xfw_module_init():
    global __native
    __native = XFWNativeModuleWrapper('com.modxvm.xfw.filewatcher', 'xfw_filewatcher.pyd', 'XFW_FileWatcher')

    global __xfw_filewatcher_initialized
    __xfw_filewatcher_initialized = True

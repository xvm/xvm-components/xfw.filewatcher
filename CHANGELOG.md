# Changelog

## v11.0.0

* MT 1.30.0 support

## v10.0.0

* First release as separate package